---
layout: page
title: node-red setup
parent: Dashboard
description: "Setting up node-red on Raspberry Pi"
nav_order: 1
permalink: /dashboard/node_red_setup
---

<a href="" target="_blank"></a>

# Description

<a href="https://nodered.org/" target="_blank">node-red</a> is a popular choice for building beautiful responsive dashboards, that visualizes data in a web browser.  
node-red is built on node.js and can be installed on different platforms.
This guide shows how to install and run node-red on a Raspberry Pi 3 running Raspbian Buster.
 
## node-red install Guide

This guide is considered a companion to the official node-red raspberry pi <a href="https://nodered.org/docs/getting-started/raspberrypi" target="_blank">install guide</a> 

1. Start your RPi and connect to a commandline via SSH
2. Open the <a href="https://nodered.org/docs/getting-started/raspberrypi" target="_blank">install guide</a> and follow it

    Install takes a while and you night need to run `sudo apt-get install build-essential` before the actual node-red install.

    <img src="../assets/images/node_red_install.png" width="500" alt="running node-red install script">   
    
    During the install you will be prompted to install RPi specific nodes, make sure to say **yes**

3. To test that node-red works, run the command `node-red-pi --max-old-space-size=256`
    During this install the RPi had both a wifi connection and a shared ethernet connection with a laptop.
    Initially it was only successfull in accessing the node red editor from the RPi's wifi ip address using `http://<hostname>:1880`
4. It is adviced to run node-red as a service when booting the pi. Description on how to do that is in the official guide.
    Test it by rebooting the RPi and open the editor as in step 3.
    After this step the editor was accessible from both the wifi and ethernet ip's.
5. To get familiar with node-red's editor try the <a href="https://nodered.org/docs/tutorials/first-flow" target="_blank">first-flow</a> tutorial and continue with the <a href="https://nodered.org/docs/tutorials/second-flow" target="_blank">second-flow</a> tutorial  
    **Note** if you are not getting any JSON data from [https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.csv](https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.csv) you can use [https://jsonplaceholder.typicode.com/posts](https://jsonplaceholder.typicode.com/posts) and remove the `CSV` node
