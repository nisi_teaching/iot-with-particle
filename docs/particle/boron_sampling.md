---
layout: page
title: Particle Boron ADC
parent: Particle
description: "Particle Boron ADC guide"
nav_order: 4
permalink: /particle/particle_adc
---

# Description

This guide explains how to set up a Boron to sample an analog voltage from one of its analog to digital (ADC) inputs.

The Boron has a 12 Bit ADC that converts an analog voltage between 0 and reference voltage (Vref) to a value between 0-4096.

The example program uses a potentiometer connected between the 3.3V pin and the GND pin on the Boron.  
This is just to generate a suitable voltage but any voltage source should work, eg. a current [loop tester with ADC interface](https://eal-itt.gitlab.io/iot-with-particle/electronics/current_adc_interface)

## Particle Boron ADC guide

1. Connect a potentiometer to the Boron as shown in *ADC connect diagram*, the value of the potentiometer is not important, it just acts as a variable voltage divider
2. From particle workbench program the Boron with the *ADC_test* program
3. Connect a serial terminal (eg. Putty) to the COM port of the Boron (in windows you can use device manager to find the COM port)
4. Turn the potentiometer, you should see a value between 0 and 4096 being written to the serial terminal, according to the position of the potentiometer as shown in *Serial terminal ADC output*

## Convert ADC value to voltage value

Looking at a raw ADC value between 0 and 4096 doesn't really say anything about what is being read by the ADC.  
Wouldn't it be nice to see the actual voltage ?  

You can convert the ADC value with the formula  
*ADC_value(Vref/4096.0)*  
and store the result in a *float* variable

To print a float use `%f` instead of `%d` in your print() statement  

Now, what's holding you back! Change that program.... 

*ADC_Test*
```C
/*
 * Project ADC_test
 * Description: Read analog value from A0 input
 * Author: NISI
 * Date: 2020-02-06
 */

int analog_in = A0; //initialize analog input A0 to variable
int analogValue; //used to store analog readings

void setup() {
  Serial.begin(9600); //used for debugging over USB
}

void loop() {
  analogValue = analogRead(analog_in); //read the analog input
  Serial.printlnf("%d", analogValue); //print the ADC value to USB
  delay(100); //blocking delay 100ms
}
```  


*ADC connect diagram*

<img src="../assets/images/potentiometer_ADC.png" width="70" alt="ADC connect diagram"> 

*Serial terminal ADC output*

<img src="../assets/images/boron_putty_ADC.png" width="250" alt="Serial terminal output ADC output"> 
