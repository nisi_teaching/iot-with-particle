---
layout: page
title: Boron 2G/3G setup
parent: Particle
description: "boron setup guide"
nav_order: 3
permalink: /particle/boron_setup
---

# Description

This page is a guide on how to do the initial setup of a particle.io Boron 2G/3G device

# Boron initial setup guide

Most of the setup is described in the guide at <a href="https://docs.particle.io/quickstart/boron/" target="_blank">https://docs.particle.io/quickstart/boron/</a>

However there are some pitfalls not described in the particle guide, the below is therefore provided as a compliment to that guide.

While setting up the device you will need to name a few things, please follow this naming convention:

```
Device name: UCL_BORON_GROUP_x (replace x with group number)  
Mesh network name: UCL_BORON_x_MESH (replace x with group number)    
```

**Remember to store your network admin password securely** 

Follow the steps:

1. Watch the <a href="https://youtu.be/ZpO0foGJ9Po" target="_blank">overview video</a>
2. After watching the video go to <a href="https://setup.particle.io/" target="_blank">https://setup.particle.io/</a> and click your way through to the Boron setup guide while performing the steps which consists of:
    1. Prepare the Hardware - Caution, the antenna is very fragile at the connector
    2. Download the particle app from app store or Google play store (android might not work for pairing, see below)
    3. Create a particle account or login with an existing account
    4. Follow app on screen instructions to pair and register the boron (pairing and updating takes a while) i never had succes doing the pairing on android, only ios?? (if using an ipad know that it's a phone app but compatible with ipad) more in this <a href="https://community.particle.io/t/unable-to-set-up-boron-lte/45166/55" target="_blank">thread</a>  
    5. Select to create a MESH network which includes 3 months of free data, but requires to enter credit card information.
3. Go to step 2 - Open web IDE <a href="https://docs.particle.io/quickstart/boron/#2-open-the-web-ide" target="_blank">https://docs.particle.io/quickstart/boron/#2-open-the-web-ide</a> 
4. If you succeed in flashing the blink sketch to the Boron, you should see the blue led next to the usb plug flashing in 1 sec intervals

**Avoid using the web ide while developing as this will require a lot of data usage.**  

To flash the Boron locally without using the web IDE you need to set up particle workbench which is described at [Particle workbench setup]({{ site.baseurl }}/particle/particle_workbench)
