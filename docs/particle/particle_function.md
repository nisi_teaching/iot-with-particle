---
layout: page
title: Particle function
parent: Particle
description: "particle function guide"
nav_order: 6
permalink: /particle/particle_function
---

# Description

This page describes how to use particle device cloud functions.

Particle functions are useful to control hardware, this can be as simple as controlling and LED or more complex ie. triggering different sampling modes of the ADC etc.

Before performing the below steps it is recommended to complete <a href="/iot-with-particle/particle/particle_cloud" target="_blank">particle cloud</a>

The below information is a supplement to the official <a href="https://docs.particle.io/reference/device-os/firmware/boron/#particle-function-" target="_blank">particle function reference</a>

## Particle function guide

1. Make a program that implements a particle cloud function

```
SYSTEM_THREAD(ENABLED); // enable system threading
int led1 = D7; //associate gpio D7 with led1

void setup() {
  Particle.function("led1", led1Control); //setup cloud function
  pinMode(led1,OUTPUT); //initialize gpio D7

}

void loop() {

}

int led1Control(String command){
  if (command == "led1_on"){
    digitalWrite(led1, HIGH); //gpio D7 on 
    Particle.publish("led1_status", "ON", 60, PRIVATE); //publish led1 status to the cloud
    return 1; //if statement executed ok
  }
  else if (command == "led1_off")
  {
    digitalWrite(led1, LOW); //gpio D7 off
    Particle.publish("led1_status", "OFF", 60, PRIVATE); //publish led1 status to the cloud
    return 2; //if statement executed ok
  }  
  else
  {
    Particle.publish("led1_status", "ERROR", 60, PRIVATE); //publish led1 status to the cloud
    return -1; //something went wrong
  }
}
```

2. If you do not have an api token, create one using the <a href="https://docs.particle.io/reference/developer-tools/cli/#particle-token-create" target="_blank">particle CLI</a>    
<img src="../assets/images/token_cli.png" width="600" alt="token cli">

3. store your token in a safe place

4. test `led1_on` with curl  
`curl https://api.particle.io/v1/devices/<your device id>/current \ -d access_token=<your access token> \ -d "args=led1_on"`  
The response should resemble:  
`{"id":"e00fce68de993b22a74276d9","last_app":"","connected":true,"return_value":1}`

5. test `led1_off` with curl  
`curl https://api.particle.io/v1/devices/<your device id>/current \ -d access_token=<your access token> \ -d "args=led1_off"`  
The response should resemble:  
`{"id":"e00fce68de993b22a74276d9","last_app":"","connected":true,"return_value":2}`

## Trigger function from node-red

1. From the particle palette add a `function` node  
<img src="../assets/images/Particle_function.png" width="100" alt="function node">  
2. Configure it:  
<img src="../assets/images/function_node.png" width="500" alt="function config">
3. Connect an inject node for led1 ON command and configure it:  
<img src="../assets/images/function_config_on.png" width="500" alt="inject on">
4. Connect an inject node for led1 OFF command and configure it:  
<img src="../assets/images/function_config_off.png" width="500" alt="inject off">
5. add an SSE node and configure it to receive status messages:  
<img src="../assets/images/function_sse.png" width="500" alt="function sse">
6. add a debug node, the finished flow should look like this:  
<img src="../assets/images/function_flow.png" width="500" alt="function_flow">
6. When clicking the inject node for ie. led1_on, you should get the same value returned from the particle function as when you tested it with Curl:  
<img src="../assets/images/function_led1on_return.png" width="300" alt="function returns 1">  
<img src="../assets/images/function_led1off_return.png" width="300" alt="function returns 2">
7. You will also get the status message from the publish command through the SSE node:  
<img src="../assets/images/function_status_on.png" width="300" alt="sse status on">   
<img src="../assets/images/function_status_off.png" width="300" alt="sse status off">

