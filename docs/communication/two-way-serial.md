---
layout: page
title: Two way serial communication
parent: Communication
description: Serial communication
nav_order: 3
---

# Serial communication

[Previously](https://eal-itt.gitlab.io/iot-with-particle/docs/communication/serial/), we looked at just receiving data.

We wish to expand that to being two way communication.

## The protocol

All text strings to and from the Boron will be of the format

```
<type>: <message>
```

We have at least these types
* `ERR`: An error where the message is the error message
* `VAL`: A value that is being returned from the boron. `<message>` will be of the form `<value id> <value>`
* `INF`: Some informational message
* `CMD`: A command, usually send from the raspberry pi to the boron. `<message>` in this context will be a string or integer depending on your design

Any expansion to this will be documented and have a three-letter type indicator.


## Connecting boron to raspberry pi

1. The example python scripts includes a ping/pong script

    It will send something specific and echo whatever it receives.

    If you have issues with the serial interfaces, see the [previous](https://eal-itt.gitlab.io/iot-with-particle/docs/communication/serial/) exercise.

2. Decide on the specifics of your protocol.

    This includes example exchanges between the boron and the raspberry and the commands that you must be able to send to the boron, e.g. `CMD: GET 01\n` to request the current value of sensor 01.   

3. Update the python program to use the correct serial port at to send the text string you decided.

4. Create the program for the boron.

    We wil use the "busy-wait" design, where there is a loop running continously and checks time and incoming data.

    The loop is something like this
    ```
    void loop() {
    	if (millis() - lastUpdate >= UPDATE_PERIOD_MS) {
    		lastUpdate = millis();
        Serial1.printlnf("sending test data %d", ++counter);
        Particle.publish( "ping", PRIVATE);
      }

      while (Serial.available()) {
        String s = Serial1.readStringUntil('\n');
        Serial1.printlnf("received data %s", s);
    	}
    }
    ```

    Freely adapted from [here](https://community.particle.io/t/serial-tutorial/26946).

5. Connect the devices and see that data i flowing.

    Perhaps you need to do some debugging before it actually works.

5. (optional) Expand the system to allow streaming of data, e.g. every 5 seconds.

    This requires a change in the send/receive strategy in the python program.
