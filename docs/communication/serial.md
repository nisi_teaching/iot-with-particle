---
layout: page
title: Serial communication
parent: Communication
description: Serial communication
nav_order: 2
---

# Serial communication

Devices may communication using different means. Serial communication is simple, and widely used.

Both boron and raspberry may be configured to use serial.

## Connecting boron to raspberry pi

1. Get the python test scripts

    It is available [here](https://gitlab.com/moozer/python-serial-generic).

    Use `git clone` on the raspberry to get it.

2. Test that it works by following the [readme.md](https://gitlab.com/moozer/python-serial-generic/blob/master/README.md)

3. Configure the raspberry pi serial interface

    See the [official documentation](https://www.raspberrypi.org/documentation/configuration/uart.md) for the steps needed.  

    This <a href="https://spellfoundry.com/2016/05/29/configuring-gpio-serial-port-raspbian-jessie-including-pi-3-4/" target="_blank">article</a> is useful if you are using a raspberry pi 3 or 4

    We want to use the GPIOs for serial communication.

4. Edit `simple_receive.py` to use the actual physical interface, e.g. /dev/ttyS0 or similar.

    See the raspberry pi documentation for the name of the interface.

5. Make a diagram showing how to connect the boron board with the raspberry.

    Boron pinouts are [here](https://docs.particle.io/assets/images/boron/boron-pinout-v1.0.pdf) and raspberry pinouts are [here](https://www.raspberrypi.org/documentation/usage/gpio/). Notice that there are differences between the various raspberry pi versions.


6. Code and upload the boron serial test program to the device.

    An 8-line example is available [here](https://community.particle.io/t/serial-tutorial/26946). This includes hints on how to use serial. Note that this is for the _photon_ board, so minor differences are to be expected.

7. Connect wires following the diagram.

8. Run `simple_receive.py` and see the data coming in.
